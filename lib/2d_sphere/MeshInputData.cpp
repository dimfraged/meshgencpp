#include "meshdefs.h"
#include <cstdio>
#include <assert.h>

// Read-in input data
void MeshInputData(double& radius, 
		   int& level)
{
  FILE* read = fopen("input.data","r");
  char buffer[256];

  int garbage;
  garbage=fscanf(read,"%lf",&radius);  
  assert(fgets(buffer, sizeof buffer, read)!=NULL);
  garbage=fscanf(read,"%i",&level);  
  assert(fgets(buffer, sizeof buffer, read)!=NULL);
  fclose(read);

  return;
}
