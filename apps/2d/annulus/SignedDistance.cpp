#include "meshdefs.h"
#include "dog_math.h"

//  Signed distance function: 
//
//      SignedDistance(x,y) < 0 inside the region
//      SignedDistance(x,y) = 0 on the boundary
//      SignedDistance(x,y) > 0 outside of the region
//
double SignedDistance(point pt)
{
  double xin = pt.x;
  double yin = pt.y;
  double rad = sqrt(xin*xin+yin*yin);
    
  double d1 = 0.5 - rad;
  double d2 = rad - 1.0;
  
  double dist = Max(d1,d2);
    
  return dist;
}
