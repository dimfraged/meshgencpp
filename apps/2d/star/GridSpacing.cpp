#include "meshdefs.h"
#include "dog_math.h"

//  Grid spacing function: 
//
//     This functions sets a relative grid spacing of points
//
//     The input is a point (i.e., x and y coordinate), and the
//     output is a real positive number called "hdist". Large (small)
//     "hdist" means relatively large (small) grid spacing (relative to
//     the maximum and minimum values of GridSpacing.cpp).
//
//     For example, GridSpacing = 1   for all input x and y and
//                  GridSpacing = 55  for all input x and y 
//
//           will produce the same nearly uniform mesh since 
//           the relative variation of GridSpacing in both
//           examples is zero.
//
double GridSpacing(point pt)
{
  double xin = pt.x;
  double yin = pt.y;
  
  double r = sqrt(pow(xin,2) +  pow(yin,2));
  double theta = atan2(yin,xin);
  
  double tmp = r-0.75e0-0.25e0*sin(5.0e0*theta);
  
  double hdist = 2.0e0 - tmp;
  
  return hdist;
}
