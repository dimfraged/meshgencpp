#include "meshdefs.h"
#include "dog_math.h"

//  Signed distance function: 
//
//      SignedDistance(x,y) < 0 inside the region
//      SignedDistance(x,y) = 0 on the boundary
//      SignedDistance(x,y) > 0 outside of the region
//
double SignedDistance(point pt)
{
  double xin = pt.x;
  double yin = pt.y;
  double r,theta;
  
  r = sqrt(pow(xin,2) +  pow(yin,2));
  theta = atan2(yin,xin);
        
  double gradx = xin/r + (5.0e0/4.0e0)*yin/(r*r)*cos(5.0*theta);
  double grady = yin/r - (5.0e0/4.0e0)*xin/(r*r)*cos(5.0*theta);
  
  double dist = (r-0.75e0-0.25e0*sin(5.0e0*theta))/sqrt(gradx*gradx+grady*grady);
  
  return dist;
}
