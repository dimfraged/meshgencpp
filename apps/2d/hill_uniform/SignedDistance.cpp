#include "meshdefs.h"
#include "dog_math.h"

//  Signed distance function: 
//
//      SignedDistance(x,y) < 0 inside the region
//      SignedDistance(x,y) = 0 on the boundary
//      SignedDistance(x,y) > 0 outside of the region
//
double SignedDistance(point pt)
{
  double xin = pt.x;
  double yin = pt.y;
  
  double d1 = 1.0e0 - yin;
  double d2 = 1.0e0 + xin;
  double d3 = 1.0e0 - xin;
  double d4 = yin - 0.25e0*exp(-25*xin*xin);

  double gradx = 12.5*xin*exp(-25*xin*xin);
  double grady = 1.0;
  d4 = d4/sqrt(gradx*gradx+grady*grady);

  double dist = -Min(Min(d1,d2),Min(d3,d4));
    
  return dist;
}
